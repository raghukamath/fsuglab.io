## Official Website of Free Software User Groups in India


## Dependencies

1. Node

## Presteps

1. Install Node ( Welcome to the world of Node <3 )
2. run `npm install -g grunt-cli`
3. run `npm install -g bower`

## Installation

1. `npm install` from root folder
2. `bower install` from root folder

## Running the application

* `grunt serve` - starts a local server and keeps watching the file changes

### Under the Hood!

Okay, Let's look at it one by one.

We installed [Node](https://nodejs.org/en/) so that we can use [Grunt](https://gruntjs.com)  ( The super awesome javascript task runnner).

But why do we even need a task runner? I'm sure you guys had fun at Selenium workshop, automating things. Grunt helps you automate a lot of things.

Now, take a look at package.json file. You'll see a lot of grunt-* dependencies each serving a special purpose in your project. eg. `grunt-contrib-concat` is for concatenating files.

Now, `Gruntfile.js`: This is where you configure/define your grunt tasks. Let's see what all things are defined there now.

Look at jshint
```
jshint: {
  main: {
      files : [{
          src : ['Gruntfile.js', 'js/**/*.js']
      }]
  },
  options: {
    globals: {
      'jQuery': true,
      'angular': true,
      'console': true,
      '$': true,
      '_': true,
      'moment': true
    }
  }
},
```
Here `main` corresponds to a subtask. That means you run this task as `grunt jshint:main`. `grunt jshint`, as you would expect, will run all the subtasks associated with jshint(In this case we've only one, ie `main`)

Now, what does this `jshint:main` task do?

It checks all the files (ie,`Gruntfile.js` and all the javascript files within js folder) for syntax errors.

Similarly we've htmlhint, [pug](https://pugjs.org/) and concat.

Now the cool part! `watch` is the task which lets you watch different files for changes and runs associated task whenever some change happens.
for example,
```
js: {
    files: 'js/**',
    options: {
        nospawn: true,
        livereload: true
    },
    tasks: ['jshint', 'concat']
},
```
This portion inside `watch` configures `jshint` and `uglify` to be run whenever files inside js folder (`js/**`) changes.

Is that a lot to take in?

Just one more! `connect` task starts a local server at [http://localhost:8080](http://localhost:8080). It also inject a script to your page that keeps a live connection to the server, so that browser will automatically refresh whenever you change and save code. Doesn't that sound awesome to you?

Let us know ;)


## License

Official Website of Free Software User Groups in India  
Copyleft (ɔ) 2017 FSCI (Free Software Community of India) - All rights reserved, all wrongs reversed   
  
This project comes under free software license: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.  
  
This project is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.  
  
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

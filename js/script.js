var mymap = L.map('mapid').setView([21, 78], 4);

L.tileLayer('//{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

var mapMarker = L.icon({
  iconUrl: 'img/marker-icon.png',
  shadowUrl: 'img/marker-shadow.png',
  iconSize: [20, 30],
  shadowSize: [30, 30],
  shadowAnchor: [10, 14]
});

// see locations.js file to add new locations

for (var i = 0; i < locations_length; i++) {

  var texttodisplay = "";
  texttodisplay += "<b>";
  texttodisplay += "";
  texttodisplay += "<br>";
  if (locations[i].URL !== "") {
    // a homepage exists
    texttodisplay += "<a href='";
    texttodisplay += locations[i].URL;
    texttodisplay += "' target='_blank' style='text-decoration:none;'>";
    texttodisplay += locations[i].TITLE;
    texttodisplay += "</a>";
  } else {
    texttodisplay += locations[i].TITLE;
  }
  texttodisplay += "<br>";
  if (locations[i].MLIST !== "") {
    // there is a mailing list for this usergroup
    texttodisplay += "<a href='";
    texttodisplay += locations[i].MLIST;
    texttodisplay += "' target='_blank' style='text-decoration:none;'>";
    texttodisplay += "Mailing List";
    texttodisplay += "</a>";
    texttodisplay += "<br>";
  }
  if (locations[i].MATURL !== "") {
    // there is a matrix room for this usergroup
    texttodisplay += "<a href='";
    texttodisplay += locations[i].MATURL;
    texttodisplay += "' target='_blank' style='text-decoration:none;'>";
    texttodisplay += "Matrix Room";
    texttodisplay += "</a>";
    texttodisplay += "<br>";
  }
  if (locations[i].TGURL !== "") {
    // not recommended
    texttodisplay += "<a href='";
    texttodisplay += locations[i].TGURL;
    texttodisplay += "' target='_blank' style='text-decoration:none;'>";
    texttodisplay += "Telegram Group";
    texttodisplay += "</a>";
    texttodisplay += "<br>";
  }
  texttodisplay += "";
  texttodisplay += "</b>";
  if ((locations[i].LAT !== "") || (locations[i].LNG !== "")) {
    L.marker([locations[i].LAT, locations[i].LNG], {
      icon: mapMarker
    }).addTo(mymap).bindPopup(texttodisplay);
  }
}

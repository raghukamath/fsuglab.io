/*
  a well structured JSON, containing the following fields:
    1. LAT: mandatory
    2. LNG: mandatory
    3. TITLE: mandatory
    4. URL: optional
    5. MLIST: optional
    6. MATURL: optional
    7. TGURL: optional
    (have I missed something?)
 */
var locations = [{
  "LAT": "18.975",
  "LNG": "72.825833",
  "TITLE": "ILUG (Indian Libre Software user Group) Bombay",
  "URL": "http://www.ilug-bom.org.in/",
  "MLIST": "http://mm.ilug-bom.org.in/mailman/listinfo/linuxers",
  "MATURL": "https://matrix.to/#/#ilug-bom:matrix.org",
  "TGURL": ""
}, {
  "LAT": "11.605",
  "LNG": "76.083",
  "TITLE": "FSUG (Free Software User Group) Wayanad",
  "URL": "",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#fsug-wayanad:matrix.org",
  "TGURL": "https://t.me/fsug_wayanad"
}, {
  "LAT": "9.49",
  "LNG": "76.33",
  "TITLE": "ILUG (Indian Libre Software User Group) Alleppey",
  "URL": "http://alpy.ilug.org.in",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#ilug-alpy:diasp.in",
  "TGURL": "https://t.me/ilug_alpy"
}, {
  "LAT": "11.8689",
  "LNG": "75.3555",
  "TITLE": "FSUG (Free Software User Group) Kannur",
  "URL": "",
  "MLIST": "https://groups.google.com/group/fsugknr",
  "MATURL": "https://matrix.to/#/#fsugknr:matrix.org",
  "TGURL": "https://t.me/fsugknr"
}, {
  "LAT": "8.88",
  "LNG": "76.60",
  "TITLE": "SSUG (Swathanthra Software Users Group) Kollam",
  "URL": "",
  "MLIST": "https://www.freelists.org/list/ssug-kollam",
  "MATURL": "https://matrix.to/#/#ssug-kollam:matrix.org",
  "TGURL": "https://t.me/ssugk"
}, {
  "LAT": "",
  "LNG": "",
  "TITLE": "ILUG (Indian Libre Software User Group) Kottayam",
  "URL": "",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#ilug-ktm:matrix.org",
  "TGURL": "https://t.me/ilugkottayam"
}, {
  "LAT": "11.25",
  "LNG": "75.78",
  "TITLE": "FSUG (Free Software User Group) Calicut",
  "URL": "http://calicut.fsug.in",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#fsug-calicut:matrix.org",
  "TGURL": "https://t.me/fsugcalicut"
}, {
  "LAT": "11.04",
  "LNG": "76.08",
  "TITLE": "SSUG (Swathanthra Software Users Group) Malappuram",
  "URL": "http://ssug.ml",
  "ORL": "http://kl10.fsug.in",
  "MLIST": "https://www.freelists.org/list/ssug-malappuram",
  "MATURL": "https://matrix.to/#/#ssugm:diasp.in",
  "TGURL": "https://t.me/ssugm"
}, {
  "LAT": "10.77",
  "LNG": "76.65",
  "TITLE": "PLUS (Palakkad Libre software Users Society)",
  "URL": "http://plus.fosscommunity.in",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#plus:matrix.org",
  "TGURL": "https://t.me/PlusFreedom"
}, {
  "LAT": "8.49",
  "LNG": "76.95",
  "TITLE": "FSUG (Free Software User Group) Thiruvananthapuram",
  "URL": "https://tvm.fsug.in",
  "MLIST": "https://groups.google.com/d/forum/ilug-tvm",
  "MATURL": "https://matrix.to/#/#fsug-tvm:matrix.org",
  "TGURL": "https://t.me/fsugtvm"
}, {
  "LAT": "10.52",
  "LNG": "76.21",
  "TITLE": "FSUG (Free Software User Group) Thrissur",
  "URL": "http://thrissur.fsug.in",
  "MLIST": "http://www.freelists.org/list/fsug-thrissur",
  "MATURL": "https://matrix.to/#/#fsug-tcr:matrix.org",
  "TGURL": "https://t.me/fsugtcr"
}, {
  "LAT": "9.97",
  "LNG": "76.28",
  "TITLE": "ILUG (Indian Libre Software User Group) Cochin",
  "URL": "http://ilugcoch.in",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#ilugcochin:matrix.org",
  "TGURL": "https://t.me/ilugcochin"
}, {
  "LAT": "10.00",
  "LNG": "76.33",
  "TITLE": "FOSSClub, Ernakulam",
  "URL": "http://fossclub.in",
  "MLIST": "",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "10.01",
  "LNG": "76.34",
  "TITLE": "RSETFC (Rajagiri School of Engineering & Technology FOSS Club), Ernakulam",
  "URL": "",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#rsetfc:matrix.org",
  "TGURL": "https://t.me/rsetfossclub"
}, {
  "LAT": "12.50",
  "LNG": "75.05",
  "TITLE": "FSUG (Free Software User Group) at LBS College of Engineering, Kasargode",
  "URL": "http://lbs.fsug.in/",
  "MLIST": "",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "10.84",
  "LNG": "76.03",
  "TITLE": "FSUG (Free Software User Group) at MES College of Engineering, Malappuram",
  "URL": "",
  "MLIST": "https://groups.google.com/forum/#!forum/mes-fsug",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "18.5293994",
  "LNG": "73.8560156",
  "TITLE": "FSUG (Free Software User Group) at College of Engineering, Pune",
  "URL": "http://co.fsug.in/",
  "MLIST": "https://groups.google.com/forum/#!forum/cofsug",
  "MATURL": "https://matrix.to/#/#cofsug:disroot.org",
  "TGURL": ""
}, {
  "LAT": "15.49",
  "LNG": "73.82",
  "TITLE": "FSUG (Free Software User Group) Goa",
  "URL": "http://goa.fsug.in",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#fsug-goa:matrix.org",
  "TGURL": ""
}, {
  "LAT": "18.519368",
  "LNG": "73.855321",
  "TITLE": "Pune GNU/Linux Users Group, Pune",
  "URL": "http://plug.org.in",
  "MLIST": "",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "28.99",
  "LNG": "77.7",
  "TITLE": "GLUG (Gnu/Linux Users Group) Meerut",
  "MLIST": "https://groups.google.com/forum/#!forum/glug-meerut",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "28.6138",
  "LNG": "77.2089",
  "TITLE": "NDBUG (New Delhi BSD User Group)",
  "URL": "http://ndbug.in",
  "MLIST": "",
  "MATURL": "",
  "TGURL": ""
}];

var locations_length = locations.length;
